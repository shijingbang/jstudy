package cn.spream.jstudy.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Created by sjx on 2015/11/6.
 */
public class NettyEncoder extends MessageToByteEncoder<Message> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out) throws Exception {
        byte[] key = msg.getKey();
        byte[] value = msg.getValue();
        int klen = key.length;
        int vlen = value.length;
        out.writeLong(msg.getId()).writeInt(klen).writeBytes(key).writeInt(vlen).writeBytes(value);
    }

}
