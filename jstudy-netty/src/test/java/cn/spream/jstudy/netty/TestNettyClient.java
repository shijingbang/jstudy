package cn.spream.jstudy.netty;

import io.netty.channel.ChannelFuture;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

/**
 * Created by sjx on 2015/11/6.
 */
public class TestNettyClient {

    private final static Log LOG = LogFactory.getLog(TestNettyClient.class);

    @Test
    public void testSend() throws InterruptedException, UnsupportedEncodingException {
        NettyClient nettyClient = new NettyClient("127.0.0.1", 8888, new LogClientNettyHandler());
        try {
            nettyClient.start();
            for (int i = 1; i <= 100; i++) {
                try {
                    Message message = new Message();
                    message.setId(i);
                    message.setKey("client".getBytes("UTF-8"));
                    message.setValue(("netty." + i).getBytes("UTF-8"));
                    ChannelFuture channelFuture = nettyClient.send(message);
                    TimeUnit.SECONDS.sleep(6);
                } catch (Exception e) {
                    LOG.error("netty send message error!", e);
                }
            }
        } finally {
            nettyClient.stop();
        }
    }


}
