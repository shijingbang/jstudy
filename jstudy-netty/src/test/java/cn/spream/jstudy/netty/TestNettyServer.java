package cn.spream.jstudy.netty;

import org.junit.Test;

/**
 * Created by sjx on 2015/11/6.
 */
public class TestNettyServer {

    @Test
    public void test() throws InterruptedException {
        NettyServer nettyServer = new NettyServer(8888, new LogServerNettyHandler());
        nettyServer.start();
    }

}
