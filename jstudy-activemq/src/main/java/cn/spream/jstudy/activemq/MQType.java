package cn.spream.jstudy.activemq;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 13-6-2
 * Time: 下午6:21
 * To change this template use File | Settings | File Templates.
 */
public enum MQType implements Serializable {

    USER("user");

    private String key;

    private MQType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
