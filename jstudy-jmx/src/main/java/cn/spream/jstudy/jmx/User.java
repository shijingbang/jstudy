package cn.spream.jstudy.jmx;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-4-14
 * Time: 上午11:29
 * To change this template use File | Settings | File Templates.
 */
public class User implements UserMBean {

    private String name;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void print() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
