package cn.spream.jstudy.designpattern.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:44
 * To change this template use File | Settings | File Templates.
 */
public class Man extends Person {

    @Override
    public void drive() {
        System.out.println("男人开车");
        getCar().run();
    }
}
