package cn.spream.jstudy.designpattern.builder;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午3:51
 * To change this template use File | Settings | File Templates.
 */
public class AsusDisplay implements Display {

    private String name = "asus";

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
