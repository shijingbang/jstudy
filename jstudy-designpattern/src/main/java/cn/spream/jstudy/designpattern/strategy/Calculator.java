package cn.spream.jstudy.designpattern.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:31
 * To change this template use File | Settings | File Templates.
 */
public interface Calculator {

    public int calculate(String exp);

}
