package cn.spream.jstudy.designpattern.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:58
 * To change this template use File | Settings | File Templates.
 */
public class InterfaceAdapterLog extends AdapterLog {

    @Override
    public void debug(String debug) {
        System.out.println("InterfaceAdapterLog.debuy：" + debug);
    }

    @Override
    public void info(String info) {
        System.out.println("InterfaceAdapterLog.info：" + info);
    }
}
