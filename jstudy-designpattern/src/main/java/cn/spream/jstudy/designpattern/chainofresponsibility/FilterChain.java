package cn.spream.jstudy.designpattern.chainofresponsibility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午1:51
 * To change this template use File | Settings | File Templates.
 */
public class FilterChain implements Filter {

    private List<Filter> filters = new ArrayList<Filter>();
    private int index;

    public FilterChain addFilter(Filter filter) {
        this.filters.add(filter);
        return this;
    }

    @Override
    public void doFilter(Request request, Response response, FilterChain chain) {
        if (index == filters.size()){
            return;
        }
        Filter filter = filters.get(index);
        index++;
        filter.doFilter(request, response, chain);
    }
}
