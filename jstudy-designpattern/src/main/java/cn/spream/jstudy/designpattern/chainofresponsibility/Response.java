package cn.spream.jstudy.designpattern.chainofresponsibility;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午1:49
 * To change this template use File | Settings | File Templates.
 */
public class Response {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
