package cn.spream.jstudy.designpattern.chainofresponsibility;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午2:00
 * To change this template use File | Settings | File Templates.
 */
public class HTMLFilter implements Filter {

    @Override
    public void doFilter(Request request, Response response, FilterChain chain) {
        String requestContent = request.getContent();
        request.setContent(requestContent.replace("<", "[").replace(">", "]"));
        chain.doFilter(request, response, chain);
        String responseContent = response.getContent();
        response.setContent(responseContent.replace("[", "<").replace("]", ">"));
    }

}
