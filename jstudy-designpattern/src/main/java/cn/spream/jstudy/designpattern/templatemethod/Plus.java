package cn.spream.jstudy.designpattern.templatemethod;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:34
 * To change this template use File | Settings | File Templates.
 */
public class Plus extends AbstractCalculator {

    @Override
    public String getOperator() {
        return "\\+";
    }

    @Override
    public int calculate(int num1, int num2) {
        return num1 + num2;
    }

}
