package cn.spream.jstudy.designpattern.chainofresponsibility;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午2:11
 * To change this template use File | Settings | File Templates.
 */
public class TestFilter {

    @Test
    public void test(){
        FilterChain filterChain = new FilterChain();
        filterChain.addFilter(new HTMLFilter()).addFilter(new FaceFilter());
        Request request = new Request();
        request.setContent("<a>:)</a>");
        Response response = new Response();
        response.setContent("[a]^V^[/a]");
        System.out.println("执行前request：" + request.getContent());
        System.out.println("执行前response：" + response.getContent());
        filterChain.doFilter(request, response, filterChain);
        System.out.println("执行后request：" + request.getContent());
        System.out.println("执行后response：" + response.getContent());
    }

}
