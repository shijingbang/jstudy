package cn.spream.jstudy.designpattern.templatemethod;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:38
 * To change this template use File | Settings | File Templates.
 */
public class TestCalculator {

    @Test
    public void testPlus() {
        Calculator calculator = new Plus();
        int result = calculator.calculate("5+4");
        System.out.println("5+4=" + result);
    }

    @Test
    public void testMinus() {
        Calculator calculator = new Minus();
        int result = calculator.calculate("5-4");
        System.out.println("5-4=" + result);
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Multiply();
        int result = calculator.calculate("5*4");
        System.out.println("5*4=" + result);
    }

}
