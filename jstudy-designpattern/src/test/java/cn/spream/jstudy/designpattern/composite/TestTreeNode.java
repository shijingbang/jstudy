package cn.spream.jstudy.designpattern.composite;

import org.junit.Test;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午4:32
 * To change this template use File | Settings | File Templates.
 */
public class TestTreeNode {

    @Test
    public void test() {
        Linux linux = new Linux();
        TreeNode root = linux.getRoot();
        print(root, "");
    }

    private void print(TreeNode treeNode, String parent) {
        List<TreeNode> childs = treeNode.getChilds();
        String name = parent + "/" + treeNode.getName();
        if (childs.size() > 0) {
            System.out.println(name);
            for (TreeNode child : childs) {
                print(child, name);
            }
        } else {
            System.out.println(name);
        }
    }

}
