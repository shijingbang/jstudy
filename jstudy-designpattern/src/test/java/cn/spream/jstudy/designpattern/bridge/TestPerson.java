package cn.spream.jstudy.designpattern.bridge;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:34
 * To change this template use File | Settings | File Templates.
 */
public class TestPerson {

    @Test
    public void test() {
        Person person = new Man();
        person.setCar(new FreightCar());
        person.drive();
        person.setCar(new SedanCar());
        person.drive();
        person = new Woman();
        person.setCar(new FreightCar());
        person.drive();
        person.setCar(new SedanCar());
        person.drive();
    }

}
