package cn.spream.jstudy.kafka.producer.service;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-8-25
 * Time: 下午3:50
 * To change this template use File | Settings | File Templates.
 */
public class KafkaMessageProducer {

    private String topic;
    private Producer kafkaProducer;

    public Future<RecordMetadata> send(final String message, final Callback callback) {
        ProducerRecord producerRecord = new ProducerRecord(topic, message);
        return kafkaProducer.send(producerRecord, callback);
    }

    public void setKafkaProducer(Producer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
