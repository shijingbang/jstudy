package cn.spream.jstudy.mongodb.domain.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * User: cn.spream
 * Date: 12-12-3
 * Time: 上午10:23
 * To change this template use File | Settings | File Templates.
 */
@Document(collection = "user")
public class User {

    @Id
    private String id;
    private String name;
    private int sex;
    private int age;
    private String mobile;
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public static enum Sex{
        MAN(1),
        WOMAN(2);

        private int key;

        private Sex(int key) {
            this.key = key;
        }

        public int getKey() {
            return key;
        }
    }
}
