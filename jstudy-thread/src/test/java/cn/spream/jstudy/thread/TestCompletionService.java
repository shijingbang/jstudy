package cn.spream.jstudy.thread;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-19
 * Time: 下午4:17
 * To change this template use File | Settings | File Templates.
 */
public class TestCompletionService {

    @Test
    public void test() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        CompletionService completionService = new ExecutorCompletionService(executorService);
        for (int i = 1; i <= 5; i++) {
            completionService.submit(new CompletionServiceCallable(String.valueOf(i)));
        }
        for (int i = 0; i < 5; i++) {
            try {
                Future<Long> future = completionService.take();
                System.out.println("获取结果成功：" + future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (ExecutionException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        System.out.println("所有任务执行完成");
    }

}
