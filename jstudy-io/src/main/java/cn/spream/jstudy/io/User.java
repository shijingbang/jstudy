package cn.spream.jstudy.io;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-9-25
 * Time: 上午10:49
 * To change this template use File | Settings | File Templates.
 */
public class User implements Serializable {

    public long id;
    public String name;
    public int age;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
